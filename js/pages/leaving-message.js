var vm = new Vue({
    el: '#app',
    data: {
        secondLevelNavList: {
            title: '留言主题列表',
            list: [
                {
                    content: '主页',
                    link: 'index.html'
                }, {
                    content: '留言',
                    link: ''
                }
            ]
        },
        leavingMessageItems: [
            {
                datetime: '2019-10-18 13:10:00',
                outline: '1.随机文本。它起源于古典拉丁语，使它超过2000年的历史。再随便打上一些文字，来看看内容多了的效果。再来一行试试呢，好像会',
                leavingUser: '17640188831'
            }, {
                datetime: '2019-10-18 13:10:00',
                outline: '2.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '17640188831'
            }, {
                datetime: '2019-10-18 13:10:00',
                outline: '3.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '17640188831'
            }, {
                datetime: '2019-10-18 13:10:00',
                outline: '4.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '17640188831'
            }, {
                datetime: '2019-10-18 13:10:00',
                outline: '5.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '17640188831'
            }, {
                datetime: '2019-10-18 13:10:00',
                outline: 'Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '17640188831'
            }, {
                datetime: '2019-10-18 13:10:00',
                outline: 'Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '17640188831'
            }, {
                datetime: '2019-10-18 13:10:00',
                outline: 'Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '17640188831'
            }, {
                datetime: '2019-10-18 13:10:00',
                outline: 'Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '17640188831'
            }, {
                datetime: '2019-10-18 13:10:00',
                outline: 'Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '17640188831'
            }, {
                datetime: '2019-10-18 13:10:00',
                outline: 'Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '17640188831'
            },
        ],
        countPerPage: 9
    },
    created() {
        $(window).on('scroll', function () {
            var scrollPos = $(this).scrollTop();
            if (scrollPos > 200) {
                $('.sticky-header').addClass('is-sticky');
            } else {
                $('.sticky-header').removeClass('is-sticky');
            }
        });
    },
    mounted() {
        $('nav.main-navigation').meanmenu({
            meanMenuClose: '<img class="black" src="img/icons/close.png" alt="close icon">',
            meanMenuCloseSize: '18px',
            meanScreenWidth: '991',
            meanExpandableChildren: true,
            meanMenuContainer: '.mobile-menu',
            onePage: true
        });
    },
    methods: {
        loadMore() {
            if (this.countPerPage < this.leavingMessageItems.length) {
                const timeout = 500;
                var _this = this;
                load(timeout);
                window.setTimeout(function () {
                    _this.countPerPage = _this.countPerPage + 6;
                }, timeout)
            } else {
                tip('没有更多了~');
            }
        }
    }
});

