let modal={
    props: {
        htmlEle: {
            type: String,
            required: true
        },
        imageObj: {
            type: Object,
            required: true
        }
    },
    template: `
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg-x" role="document">
                <div class="modal-content modal-transparent">
                    <div class="modal-header modal-transparent-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="fa fa-close text-danger"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid text-center">
                            <span v-html="htmlEle"></span>
                        </div>
                        <div class="container-fluid text-center photo-list">
                            <div class="row">
                                <span class="col-lg-1"></span>
                                <span class="col-lg-3 photo-list-basic">
                                    <span class="row">
                                        <span class="col-lg-12 text-left">
                                            <i class="fa fa-calendar"></i>日期:{{imageObj.datetime|date-diff}}
                                        </span>
                                    </span>
                                    <span class="row">
                                        <span class="col-lg-12 text-left">
                                            <i class="fa fa-eye"></i>浏览:{{imageObj.view}}
                                        </span>
                                    </span>
                                    <span class="row">
                                        <span class="col-lg-12 text-left">
                                            <i class="fa fa-lock"></i>类型:{{imageObj.lock?'加密':'公开'}}
                                        </span>
                                    </span>
                                    <span class="row">
                                        <span class="col-lg-12"></span>
                                    </span>
                                </span>
                                <span class="col-lg-7 photo-list-detail">
                                    <span class="row">
                                        <p class="col-lg-12">
                                            {{imageObj.photoGroup}}
                                        </p>
                                    </span>
                                    <span class="row">
                                        <p class="col-lg-12">
                                            {{imageObj.photoName}}
                                        </p>
                                    </span>
                                    <span class="row">
                                        <p class="col-lg-12">
                                            {{imageObj.description}}
                                        </p>
                                    </span>
                                    <span class="row">
                                        <p class="col-lg-12">
                                            <span class="row tags">
                                                <span class="col-lg-1">标签:</span>
                                                <span class="col-lg-11">
                                                    <span v-random-bg-tag v-for="tag in imageObj.tags" class="col-lg-1">{{tag}}</span>
                                                </span>
                                            </span>
                                        </p>
                                    </span>
                                </span>
                                <span class="col-lg-1"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `
}

new Vue({
    el: '#app',
    data: {
        secondLevelNavList: {
            title: '相册',
            list: [
                {
                    content: '主页',
                    link: 'index.html'
                }, {
                    content: '相册列表',
                    link: 'photos.html'
                }, {
                    content: '相册',
                    link: ''
                }
            ]
        },
        photos: [
            {
                src: 'img/portfolio/details/portfolio-details-image-1.jpg',
                datetime: '2019-10-20 00:00:00',
                view: 2,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '0照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            },
            {
                src: 'img/portfolio/lg-size/portfolio-image-lg-1.jpg',
                datetime: '2019-10-19 00:00:00',
                view: 4,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '1照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-2.jpg',
                datetime: '2019-10-19 00:00:00',
                view: 6,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '2照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-3.jpg',
                datetime: '2019-10-18 00:00:00',
                view: 8,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '3照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-4.jpg',
                datetime: '2019-10-17 00:00:00',
                view: 10,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '4照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-5.jpg',
                datetime: '2019-10-16 00:00:00',
                view: 12,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '5照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg',
                datetime: '2019-10-15 00:00:00',
                view: 14,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '6照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg',
                datetime: '2019-10-14 00:00:00',
                view: 16,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '7照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg',
                datetime: '2019-10-13 00:00:00',
                view: 18,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '8照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg',
                datetime: '2019-10-12 00:00:00',
                view: 20,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '9照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg',
                datetime: '2019-10-11 00:00:00',
                view: 22,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '10照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg',
                datetime: '2019-10-10 00:00:00',
                view: 24,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '11照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg',
                datetime: '2019-10-09 00:00:00',
                view: 26,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '12照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg',
                datetime: '2019-10-08 00:00:00',
                view: 28,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '13照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg',
                datetime: '2019-10-07 00:00:00',
                view: 30,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '14照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg',
                datetime: '2019-10-06 00:00:00',
                view: 32,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '15照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg',
                datetime: '2019-10-05 00:00:00',
                view: 34,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '16照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            }, {
                src: 'img/portfolio/lg-size/portfolio-image-lg-17.jpg',
                datetime: '2019-10-04 00:00:00',
                view: 36,
                lock: false,
                photoGroup: '测试相册名称',
                photoName: '17照片名称',
                description: '照片的描述信息',
                tags: ['旅游', '家庭', '贵州', '山水']
            },

        ],
        isRoll: false,
        psr: {
            cursorOffset: 0,
            eventObject: {},
            num: 0,
            position: ''
        },
        defaultImage: {},
        content: ''
    },
    components:{
        modal
    },
    methods: {
        toggleType(obj) {
            let target = obj.target.id;
            if (target === 'roll' && !this.isRoll) {
                this.isRoll = true;
            }
            if (target === 'list' && this.isRoll) {
                this.isRoll = false;
            }
        },
        changeImage(eventObj) {
            if (this.isRoll) {
                if (this.psr.position === 'left') {
                    if (this.psr.num > 0) {
                        this.psr.num--;
                    } else {
                        tip('已经是第一张了~');
                    }
                } else {
                    if (this.psr.num < (this.photos.length - 1)) {
                        this.psr.num++;
                    } else {
                        tip('已经是最后一张了');
                    }
                }
                this.defaultImage = this.photos[this.psr.num];
            } else {
                $("#myModal").modal('show');
                this.content = '<span class="col-md-12 text-center">' +
                    '<img src="' + eventObj.src + '">' +
                    '</span>'
                this.defaultImage = eventObj;
            }
        }
    },
    watch: {
        isRoll(newValue) {
            cc.info("发生变化:" + newValue)
        },
        getCursorOffset(newValue) {
            let clientWidth = this.psr.eventObject.clientWidth;
            if (newValue > 0 && clientWidth > 0) {
                this.psr.position = (newValue <= (clientWidth / 2)) ? "left" : "right";
                this.psr.eventObject.style.cursor = "url('./img/arrow/arrow-" + this.psr.position + ".png'), default";
            }
        }
    },
    computed: {
        getCursorOffset() {
            return this.psr.cursorOffset;
        }
    },
    mounted() {
        this.defaultImage = this.photos[0];
    },
    beforeDestroy() {
        this.psr.eventObject = {};
        this.psr.cursorOffset = 0;
        this.psr.position = '';
        cc.warn('数据销毁，变量已重置');
    }
});
