var vm = new Vue({
    el: '#app',
    data: {
        secondLevelNavList: {
            title: '文章的标题',
            list: [
                {
                    content: '主页',
                    link: 'index.html'
                }, {
                    content: '文章列表',
                    link: 'article.html'
                }, {
                    content: '文章内容',
                    link: ''
                }
            ]
        },
        articleDetail: {
            head: {
                isTop: true,
                img: 'img/blog/blog-image-1.jpg',
                imgTo: 'article-details.html',
                imgAlt: 'blog thumb',
            },
            body: {
                title: '1.浏览须知浏览须知浏览须知浏览须',
                titleTo: 'article-details.html',
                outline: '目前有部分浏览者盗用原创作品,在此声明:未经本人允许目前有部分浏览者盗用原创作品,在此声明:未经本人允许目前有部分浏览者盗用原创作品,在此声明:未经本人允许目前有部分浏览者盗用原创作品,在此声明:未经许许许'
            },
            foot: {
                favor: 10,
                writeDate: '2014-04-23 14:28:00',
                comments: 3
            }
        },
        itemLeavingMessageList: [
            {
                userHeadImage: 'img/author-image/author-image-1.png',
                userName: '17640188831',
                userLeavingDatetime: 'February 17, 2015',
                replyButton: true,
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue\n                                            nec est tristique auctor. Donec non est at libero vulputate rutrum.',
                replyForReply:false
            }, {
                userHeadImage: 'img/author-image/author-image-2.png',
                userName: '17640188832',
                userLeavingDatetime: 'February 17, 2015',
                replyButton: true,
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue\n                                            nec est tristique auctor. Donec non est at libero vulputate rutrum.',
                replyForReply:true
            }, {
                userHeadImage: 'img/author-image/author-image-1.png',
                userName: '17640188833',
                userLeavingDatetime: 'February 17, 2015',
                replyButton: true,
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue\n                                            nec est tristique auctor. Donec non est at libero vulputate rutrum.',
                replyForReply:false
            },
        ]
    }
});
