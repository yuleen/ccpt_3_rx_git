var vm = new Vue({
    el: '#app',
    data: {
        articleItems: [
            {
                head: {
                    isTop: true,
                    img: 'img/blog/blog-image-1.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '1.浏览须知浏览须知浏览须知浏览须',
                    titleTo: 'article-details.html',
                    outline: '目前有部分浏览者盗用原创作品,在此声明:未经本人允许目前有部分浏览者盗用原创作品,在此声明:未经本人允许目前有部分浏览者盗用原创作品,在此声明:未经本人允许目前有部分浏览者盗用原创作品,在此声明:未经许许许'
                },
                foot: {
                    favor: 10,
                    writeDate: '2014-04-23 14:28:00',
                    comments: 3
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-2.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '2.自己建一个个人主页',
                    titleTo: 'article-details.html',
                    outline: '自己写一个个人主页，前端使用vue.js顺便了解下VUE.JS的语法，还在写，这个事凑数的'
                },
                foot: {
                    favor: 100,
                    writeDate: '2019-10-02 13:22:00',
                    comments: 6
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-3.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '3.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 60,
                    writeDate: '1986-06-27 06:00:00',
                    comments: 9
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-4.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '4.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 60,
                    writeDate: '1983-11-06 22:00:00',
                    comments: 12
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-5.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '5.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 30,
                    writeDate: '2019-09-31 22:00',
                    comments: 15
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '6.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '7.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '8.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '9.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '10.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '11.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '12.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '13.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '14.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '15.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '16.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '17.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '18.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '19.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '20.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '21.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '22.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '23.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '24.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '25.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '26.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '27.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '28.Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            }
        ],
        secondLevelNavList: {
            title: '文章列表',
            list: [
                {
                    content: '主页',
                    link: 'index.html'
                }, {
                    content: '文章列表',
                    link: ''
                }
            ]
        },
        countPerPage: 9
    },
    created() {
        $(window).on('scroll', function () {
            var scrollPos = $(this).scrollTop();
            if (scrollPos > 200) {
                $('.sticky-header').addClass('is-sticky');
            } else {
                $('.sticky-header').removeClass('is-sticky');
            }
        });
    },
    mounted() {
        $('nav.main-navigation').meanmenu({
            meanMenuClose: '<img class="black" src="img/icons/close.png" alt="close icon">',
            meanMenuCloseSize: '18px',
            meanScreenWidth: '991',
            meanExpandableChildren: true,
            meanMenuContainer: '.mobile-menu',
            onePage: true
        });
    },
    methods: {
        startSearch() {
            console.log('传递成功');
        },
        loadMore() {
            if (this.countPerPage < this.articleItems.length) {
                const timeout = 500;
                var _this = this;
                load(timeout);
                window.setTimeout(function () {
                    _this.countPerPage = _this.countPerPage + 6;
                }, timeout)
            } else {
                tip('没有更多了~');
            }
        }
    }
});

