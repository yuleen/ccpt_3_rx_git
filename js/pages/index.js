var vm = new Vue({
    el: '#app',
    data: {
        content: 'Powered by Leen',
        signature: '狂狼是一种态度',
        currentHeart: '微笑面对各种2B',
        count: {
            visit: 10000,
            share: 90,
            article: 20
        },
        countPerPage: 6,
        defaultLeavingMessageVisibleCount: 9,
        articleItems: [
            {
                head: {
                    isTop: true,
                    img: 'img/blog/blog-image-1.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '浏览须知浏览须知浏览须知浏览须',
                    titleTo: 'article-details.html',
                    outline: '目前有部分浏览者盗用原创作品,在此声明:未经本人允许目前有部分浏览者盗用原创作品,在此声明:未经本人允许目前有部分浏览者盗用原创作品,在此声明:未经本人允许目前有部分浏览者盗用原创作品,在此声明:未经许许许'
                },
                foot: {
                    favor: 10,
                    writeDate: '2014-04-23 14:28:00',
                    comments: 3
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-2.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: '自己建一个个人主页',
                    titleTo: 'article-details.html',
                    outline: '自己写一个个人主页，前端使用vue.js顺便了解下VUE.JS的语法，还在写，这个事凑数的'
                },
                foot: {
                    favor: 100,
                    writeDate: '2019-10-02 13:22:00',
                    comments: 6
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-3.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: 'Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 60,
                    writeDate: '1986-06-27 06:00:00',
                    comments: 9
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-4.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: 'Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 60,
                    writeDate: '1983-11-06 22:00:00',
                    comments: 12
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-5.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: 'Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 30,
                    writeDate: '2019-09-31 22:00',
                    comments: 15
                }
            }, {
                head: {
                    isTop: false,
                    img: 'img/blog/blog-image-6.jpg',
                    imgTo: 'article-details.html',
                    imgAlt: 'blog thumb',
                },
                body: {
                    title: 'Accusamus iste quia qui quisquam qui',
                    titleTo: 'article-details.html',
                    outline: 'Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum\n                                           itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio...'
                },
                foot: {
                    favor: 44,
                    writeDate: '2018-10-01 21:25:00',
                    comments: 18
                }
            },
        ],
        leavingMessageItems: [
            {
                datetime: '刚刚',
                outline: '1.随机文本。它起源于古典拉丁语，使它超过2000年的历史。再随便打上一些文字，来看看内容多了的效果。再来一行试试呢，好像会',
                leavingUser: '游客22456'
            }, {
                datetime: '三天前',
                outline: '2.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '游客55286'
            }, {
                datetime: '五天前',
                outline: '3.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '游客33456'
            }, {
                datetime: '一周前',
                outline: '4.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '游客55789'
            }, {
                datetime: '一个月前',
                outline: '5.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '游客24567'
            }, {
                datetime: '一年前',
                outline: '6.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '游客22456'
            },{
                datetime: '一年前',
                outline: '7.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '游客22456'
            },{
                datetime: '一年前',
                outline: '8.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '游客22456'
            },{
                datetime: '一年前',
                outline: '9.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '游客22456'
            },{
                datetime: '一年前',
                outline: '10.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '游客22456'
            },{
                datetime: '一年前',
                outline: '11.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '游客22456'
            },{
                datetime: '一年前',
                outline: '12.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '游客22456'
            },{
                datetime: '一年前',
                outline: '13.Random text. It has roots in classical Latin making it over 2000 years old.',
                leavingUser: '游客22456'
            },
        ],
        photoItems: [
            {
                smallPic: 'img/portfolio/portfolio-image-1.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-1.jpg',
                title: '时间就是生命',
                datetime: '2019-10-01 00:00:00',
                needPassword: false,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-1.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-2.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-3.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-4.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-5.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-2.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-2.jpg',
                title: '白炽灯',
                datetime: '2019-09-01 00:00:00',
                needPassword: true,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-2.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-3.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-4.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-5.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-3.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-3.jpg',
                title: '人生',
                datetime: '2019-08-01 00:00:00',
                needPassword: false,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-3.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-4.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-5.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-4.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-4.jpg',
                title: '随便起个名字',
                datetime: '2019-09-01 00:00:00',
                needPassword: true,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-4.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-5.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-5.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-5.jpg',
                title: '叫啥呢',
                datetime: '2019-09-01 00:00:00',
                needPassword: false,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-5.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-6.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg',
                title: '锄禾日当午',
                datetime: '2019-09-01 00:00:00',
                needPassword: false,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-7.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg',
                title: '汗滴禾下土',
                datetime: '2019-09-01 00:00:00',
                needPassword: true,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-8.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg',
                title: '谁知盘中餐',
                datetime: '2019-09-01 00:00:00',
                needPassword: false,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-9.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg',
                title: '粒粒皆辛苦',
                datetime: '2019-09-01 00:00:00',
                needPassword: false,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            }
        ]
    },
    created() {
        $(window).on('scroll', function () {
            var scrollPos = $(this).scrollTop();
            if (scrollPos > 200) {
                $('.sticky-header').addClass('is-sticky');
            } else {
                $('.sticky-header').removeClass('is-sticky');
            }
        });
    },
    mounted() {
        $('nav.main-navigation').meanmenu({
            meanMenuClose: '<img class="black" src="img/icons/close.png" alt="close icon">',
            meanMenuCloseSize: '18px',
            meanScreenWidth: '991',
            meanExpandableChildren: true,
            meanMenuContainer: '.mobile-menu',
            onePage: true
        });

        $('.visit-counter').counterUp({
            delay: 10,
            time: 1000
        });
        $('.publish-counter').counterUp({
            delay: 10,
            time: 1000
        });
        $('.share-counter').counterUp({
            delay: 10,
            time: 1000
        });
    },
    methods: {
        shareWebsite: function () {
            var copyObject = new ClipboardJS(".share", {
                text: function () {
                    return '我发现个网站内容还不错!要不要看看?' + window.location.href;
                }
            })
            copyObject.on('success',
                function (e) {
                    tip("复制成功");
                    copyObject.destroy();
                });
        },
    }
});

