var vm = new Vue({
    el: '#app',
    data: {
        secondLevelNavList: {
            title: '相册列表',
            list: [
                {
                    content: '主页',
                    link: 'index.html'
                }, {
                    content: '相册列表',
                    link: ''
                }
            ]
        },
        countPerPage: 6,
        photoItems: [
            {
                smallPic: 'img/portfolio/portfolio-image-1.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-1.jpg',
                title: '时间就是生命',
                datetime: '2019-10-01 00:00:00',
                needPassword: false,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-1.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-2.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-3.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-4.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-5.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-2.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-2.jpg',
                title: '白炽灯',
                datetime: '2019-09-01 00:00:00',
                needPassword: true,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-2.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-3.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-4.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-5.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-3.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-3.jpg',
                title: '人生',
                datetime: '2019-08-01 00:00:00',
                needPassword: false,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-3.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-4.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-5.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-4.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-4.jpg',
                title: '随便起个名字',
                datetime: '2019-09-01 00:00:00',
                needPassword: true,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-4.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-5.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-5.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-5.jpg',
                title: '叫啥呢',
                datetime: '2019-09-01 00:00:00',
                needPassword: false,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-5.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-6.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg',
                title: '锄禾日当午',
                datetime: '2019-09-01 00:00:00',
                needPassword: false,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-6.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-7.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg',
                title: '汗滴禾下土',
                datetime: '2019-09-01 00:00:00',
                needPassword: true,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-7.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-8.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg',
                title: '谁知盘中餐',
                datetime: '2019-09-01 00:00:00',
                needPassword: false,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-8.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            },
            {
                smallPic: 'img/portfolio/portfolio-image-9.jpg',
                largePic: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg',
                title: '粒粒皆辛苦',
                datetime: '2019-09-01 00:00:00',
                needPassword: false,
                items: [
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-9.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-10.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-11.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-12.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-13.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-14.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-15.jpg'},
                    {src: 'img/portfolio/lg-size/portfolio-image-lg-16.jpg'}
                ]
            }
        ]
    },
    created() {
        $(window).on('scroll', function () {
            var scrollPos = $(this).scrollTop();
            if (scrollPos > 200) {
                $('.sticky-header').addClass('is-sticky');
            } else {
                $('.sticky-header').removeClass('is-sticky');
            }
        });
    },
    mounted() {
        $('nav.main-navigation').meanmenu({
            meanMenuClose: '<img class="black" src="img/icons/close.png" alt="close icon">',
            meanMenuCloseSize: '18px',
            meanScreenWidth: '991',
            meanExpandableChildren: true,
            meanMenuContainer: '.mobile-menu',
            onePage: true
        });
    },
    methods: {
        loadMore() {
            if (this.countPerPage < this.photoItems.length) {
                const timeout = 500;
                var _this = this;
                load(timeout);
                window.setTimeout(function () {
                    _this.countPerPage = _this.countPerPage + 6;
                }, timeout)
            } else {
                tip('没有更多了~');
            }
        }
    }
});
