let timeline = {
    props: {
        timelineList: {
            type: Array,
            required: true
        }
    },
    template: `
                        <div class="timeline">
                            <div class="timeline-item" :class="index==0?'timeline-item-left':(index%2==0)?'timeline-item-left':'timeline-item-right'" v-for="(timeline,index) in timelineList">
                                <div class="timeline-point timeline-point-primary">
                                    <i class="fa fa-heart"></i>
                                </div>
                                <div class="timeline-event timeline-event-default">
                                    <div class="timeline-heading">
                                        <h4>{{timeline.title}}</h4>
                                    </div>
                                    <div class="timeline-body">
                                        <p>{{timeline.body}}</p>
                                    </div>
                                    <div class="timeline-footer">
                                        <p class="text-right">{{timeline.footer}}</p>
                                    </div>
                                </div>
                            </div>

                            <span class="timeline-label">
                                <button class="btn btn-danger"><i class="fa fa-ambulance"></i></button>
                            </span>
                        </div>
    `
}


new Vue({
    el: '#app',
    data: {
        secondLevelNavList: {
            title: '我的心情',
            list: [
                {
                    content: '主页',
                    link: 'index.html'
                }, {
                    content: '心情',
                    link: ''
                }
            ]
        },
        currentItem: {},
        feelingList: [
            {
                title: 'timeline-event timeline-event-default',
                body: '这是第一个',
                footer: 'Feb-21-2014'
            }, {
                title: 'timeline-event timeline-event-primary',
                body: '这是第二个',
                footer: 'Feb-23-2014'
            }, {
                title: 'timeline-event timeline-event-info',
                body: '3-Advenientibus aliorum eam ad per te sed. Facere debetur aut veneris accedens.',
                footer: 'Feb-22-2014'
            }, {
                title: 'timeline-event timeline-event-warning',
                body: '4-Stranguillione in deinde cepit roseo commendavit patris super color est se sed. Virginis plus plorantes abscederem assignato ipsum ait regem Ardalio nos filiae Hellenicus mihi cum. Theophilo litore in lucem in modo invenit quasi nomen magni ergo est se est Apollonius, habet clementiae venit ad nomine sed dominum depressit filia navem.',
                footer: 'Feb-23-2014'
            }, {
                title: 'timeline-event timeline-event-danger',
                body: '5-Advenientibus aliorum eam ad per te sed. Facere debetur aut veneris accedens.',
                footer: 'Feb-22-2014'
            }, {
                title: 'timeline-item timeline-item-arrow-sm',
                body: '6-Volvitur ingreditur id ait mea vero cum autem quod ait Cumque ego illum vero cum unde beata. Commendavi si non dum est in. Dionysiadem tuos ratio puella ut casus, tunc lacrimas effunditis magister cives Tharsis. Puellae addita verbaque\' capellam sanctissima quid, apollinem existimas filiam rex cum autem quod tamen adnuente rediens eam est se in. Peracta licet ad nomine Maria non ait in modo compungi mulierem volutpat.',
                footer: 'Feb-27-2014'
            }, {
                title: 'timeline-point timeline-point-blank',
                body: '8-Crede respiciens loco dedit beneficio ad suis alteri si puella eius in. Acceptis codicello lenonem in deinde plectrum anni ipsa quod eam est Apollonius.',
                footer: 'Mar-02-2014'
            }, {
                title: 'timeline-item timeline-item-left',
                body: '9-Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.',
                footer: 'Feb-21-2014'
            }, {
                title: 'timeline-item timeline-item-left',
                body: '10-Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.',
                footer: 'Feb-21-2014'
            }, {
                title: 'timeline-item timeline-item-left',
                body: '11-Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.',
                footer: 'Feb-21-2014'
            }, {
                title: 'timeline-item timeline-item-left',
                body: '12-Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.',
                footer: 'Feb-21-2014'
            }, {
                title: 'timeline-item timeline-item-right',
                body: '13-Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.',
                footer: 'Feb-21-2014'
            }, {
                title: 'timeline-item timeline-item-right',
                body: '14-Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.',
                footer: 'Feb-21-2014'
            }, {
                title: 'timeline-item timeline-item-right',
                body: '15-Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.',
                footer: 'Feb-21-2014'
            }, {
                title: 'timeline-item timeline-item-right',
                body: '16-Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.',
                footer: 'Feb-21-2014'
            }, {
                title: 'timeline-item',
                body: '17-Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.',
                footer: 'Feb-21-2014'
            }, {
                title: 'timeline-item',
                body: '18-Invitamus me testatur sed quod non dum animae tuae lacrimis ut libertatem deum rogus aegritudinis causet. Dicens hoc contra serpentibus isto.',
                footer: 'Feb-21-2014'
            }
        ]
    },
    components: {
        timeline
    },
    created() {
        $(window).on('scroll', function () {
            var scrollPos = $(this).scrollTop();
            if (scrollPos > 200) {
                $('.sticky-header').addClass('is-sticky');
            } else {
                $('.sticky-header').removeClass('is-sticky');
            }
        });
    },
    mounted() {
        $('nav.main-navigation').meanmenu({
            meanMenuClose: '<img class="black" src="img/icons/close.png" alt="close icon">',
            meanMenuCloseSize: '18px',
            meanScreenWidth: '991',
            meanExpandableChildren: true,
            meanMenuContainer: '.mobile-menu',
            onePage: true
        });
    }
});
