let modal = {
    data() {
        return {
            current: {}
        }
    },
    template: `
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><strong>{{this.$parent.currentItem.title}}</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="fa fa-close text-danger"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid text-center photo-list">
                    <div class="row">
                        <span class="col-lg-12">
                            <span v-html="this.$parent.currentItem.content"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>
    `
}
let headers = {
    template: `
        <div class="col-md-12 col-sm-12 text-center">
                            <img src="img/author-image/head-img.jpg" class="img-circle">
                            <hr>
                            <h3 class="info-1">大家好，我是某某人</h3>
                            <h3 class="info-2">后端开发</h3>
                        </div>
    `
}
let detail = {
    props: {
        styles: {
            type: String,
            required: false
        }
    },
    template: `
        <div class="row" :class="styles">
             <span class="col-lg-12 text-right">
                   <i class="fa fa-ellipsis-h fa-2x" @click="$emit('open-modal')"></i>
             </span>
        </div>
    `
}
let aboutMe = {
    props: {
        resume: {
            type: Object,
            required: true
        }
    },
    components: {
        detail,
        headers
    },
    template: `
            <div class="cr-section section-padding-sm">
                <div class="container">
                    <div class="row">
                           <headers></headers>
                    </div>  
                </div>
                <section class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="about">
                                <h2 class="accent">{{resume.about.title}}</h2>
                                <span v-html="resume.about.content"></span>
                                <detail :styles="resume.about.styles" @open-modal="openModal(resume.about)" ></detail>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="skills">
                                <h2 class="white">{{resume.skills.title}}</h2>
                                <span v-html="resume.skills.content"></span>
                                <detail :styles="resume.skills.styles"  @open-modal="openModal(resume.skills)"></detail>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-12">
                            <div class="education">
                                <h2 class="white">{{resume.education.title}}</h2>
                                <span v-html="resume.education.content"></span>
                                <detail :styles="resume.education.style"  @open-modal="openModal(resume.education)"></detail>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="languages">
                                <h2>{{resume.languages.title}}</h2>
                                <span v-html="resume.languages.content"></span>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="contact">
                                <h2>{{resume.contact.title}}</h2>
                                <span v-html="resume.contact.content"></span>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12">
                            <div class="experience">
                                <h2 class="white">{{resume.experience.title}}</h2>
                                <span v-html="resume.experience.content"></span>
                                <detail :styles="resume.experience.styles"  @open-modal="openModal(resume.experience)"></detail>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
    `,
    methods: {
        openModal(event) {
            $("#myModal").modal('show');
            this.$parent.currentItem = event;
        }
    }
}

new Vue({
    el: '#app',
    data: {
        secondLevelNavList: {
            title: '我的简历',
            list: [
                {
                    content: '主页',
                    link: 'index.html'
                }, {
                    content: '简历',
                    link: ''
                }
            ]
        },
        aboutMe: {
            about: {
                styles: 'about-detail',
                title: '项目经验',
                content: '<p>This easy HTML profile is brought to you by templatemo website. There are 4 color themes, <a href="index-green.html">Green</a>, <a href="index.html">Blue</a>, <a href="index-gray.html">Gray</a>, and <a href="index-orange.html">Orange</a>. Sed vitae dui in neque elementum tempor eu id risus. Phasellus sed facilisis lacus, et venenatis augue.</p>'
            },
            skills: {
                styles: 'skills-detail',
                title: '技能',
                content: '<strong>PHP MySQL</strong>' +
                    '<span class="pull-right">70%</span>' +
                    '<div class="progress">' +
                    '<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;"></div>' +
                    '</div>' +
                    '<strong>UI/UX Design</strong>' +
                    '<span class="pull-right">85%</span>' +
                    '<div class="progress">' +
                    '<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;"></div>' +
                    '</div>' +
                    '<strong>Bootstrap</strong>' +
                    '<span class="pull-right">95%</span>' +
                    '<div class="progress">' +
                    '<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;"></div>' +
                    ' </div>'
            },
            education: {
                styles: 'education-detail',
                title: '教育经历',
                content: '<div class="row">' +
                    '<span class="col-lg-12">长春大学</span>' +
                    '<span class="col-lg-12">2002.06 - 2006-09</span>' +
                    '<span class="col-lg-12">' +
                    '啊但是发射点发大水发射点发到付爱的发大水发射点发射点发射点发的啊打发撒旦发射点发生,啊但是发射点发大水发射点发到付爱的发大水发射点发射点发射点发的啊打发撒旦发射点发生' +
                    '</span>' +
                    '</div>'
            },
            languages: {
                styles: '',
                title: '语言特长',
                content: '<ul>' +
                    '<li>Myanmar / Thai</li>' +
                    '<li>English / Spanish</li>' +
                    '<li>Chinese / Japanese</li>' +
                    '<li>Arabic / Hebrew</li>' +
                    '</ul>'
            },
            contact: {
                styles: '',
                title: '联系方式',
                content: '<p><i class="fa fa-map-marker"></i> 123 Rama IX Road, Bangkok</p>' +
                    '<p><i class="fa fa-phone"></i> 010-020-0890</p>' +
                    '<p><i class="fa fa-envelope"></i> easy@company.com</p>' +
                    '<p><i class="fa fa-globe"></i> www.company.com</p>'
            },
            experience: {
                styles: 'experience-detail',
                title: '工作经历',
                content: '<div class="row">' +
                    '<span class="col-lg-12">长春大学</span>' +
                    '<span class="col-lg-12">2002.06 - 2006-09</span>' +
                    '<span class="col-lg-12">' +
                    '啊但是发射点发大水发射点发到付爱的发大水发射点发射点发射点发的啊打发撒旦发射点发生,啊但是发射点发大水发射点发到付爱的发大水发射点发射点发射点发的啊打发撒旦发射点发生' +
                    '</span>' +
                    '</div>'
            }
        },
        currentItem: {}
    },
    components: {
        'about-me': aboutMe,
        modal
    },
    created() {
        $(window).on('scroll', function () {
            var scrollPos = $(this).scrollTop();
            if (scrollPos > 200) {
                $('.sticky-header').addClass('is-sticky');
            } else {
                $('.sticky-header').removeClass('is-sticky');
            }
        });
    },
    mounted() {
        $('nav.main-navigation').meanmenu({
            meanMenuClose: '<img class="black" src="img/icons/close.png" alt="close icon">',
            meanMenuCloseSize: '18px',
            meanScreenWidth: '991',
            meanExpandableChildren: true,
            meanMenuContainer: '.mobile-menu',
            onePage: true
        });
    }
});
