'use strict';
const success = '#28a745',
    warn = '#ffc107',
    error = '#dc3545',
    info = '#17a2b8';

var cc = {
    success(msg) {
        console.log('%c ' + msg, 'color:' + success);
    },
    warn(msg) {
        console.log('%c ' + msg, 'color:' + warn);
    },
    error(msg) {
        console.log('%c ' + msg, 'color:' + error);
    },
    info(msg) {
        console.log('%c ' + msg, 'color:' + info);
    }
}
