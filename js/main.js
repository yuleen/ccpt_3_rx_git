/*********************************************************************************

	Template Name: Karigor - Minimalist Bootstrap4 Portfolio Template
	Description: A perfect minimal template to build beautiful and unique portfolio websites. It comes with nice and clean design.
	Version: 1.0

	Note: This is main js.

**********************************************************************************/

/**************************************************************

	STYLESHEET INDEXING
	|
	|
	|___ Sticky Header
	|___ Testimonial Slider Active
	|___ Bootstrap4 Tooltip Active
	|___ Portfolio Filter & Popup Active
	|___ Header Menu Effect
	|___ Mobile Menu
	|___ Boxed Layout
	|___ Counter Active
	|___ Radial Progress
	|___ Blog Item Height
	|___ Sticky Sidebar
	|
	|
	|___ END STYLESHEET INDEXING

***************************************************************/


(function ($) {
	'use strict';
	/* Testimonial Slider Active */
	$('.testimonial-slider').slick({
		slidesToShow: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		dots: true,
		arrows: false,
		easing: 'ease-in-out',
	});





	/* Bootstrap4 Tooltip Active */
	// $('[data-toggle="tooltip"]').tooltip();






	/* Counter Active */
	$('.counter-active').counterUp({
		delay: 10,
		time: 1000
	});





	/* Radial Progress */
	$('.radial-progress').waypoint(function(){

		$('.radial-progress').easyPieChart({
			lineWidth: 3,
			trackColor: false,
			scaleLength: 0,
			rotate: -45,
			barColor: '#555555'
		});

	}, {
		triggerOnce: true,
		offset: 'bottom-in-view'
	});




	/* Blog Item Height */
	$('.blog-item').matchHeight();




	/* Sticky Sidebar */
	$('.sticky-sidebar-active').theiaStickySidebar({
		additionalMarginTop: 30,
		additionalMarginBottom: 30
	});




})(jQuery);
