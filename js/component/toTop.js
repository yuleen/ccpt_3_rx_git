/**
 * 返顶组件
 */
Vue.component('to-top', {
    template: `
    <div id="retTop" class="bottom-right-icon">
        <a class="bottom-right-icon-background fa fa-chevron-up fa-lg text-dark" title="返回顶部" href="#top" name="top"></a>
    </div>
    `,
    mounted() {
        $('#app').prepend('<a name="top"></a>')
    }
})
