/**
 * 文章列表组件
 */
Vue.component('article-list', {
    props: {
        items: {
            type: Array,
            required: true
        },
        countPerPage: {
            type: Number,
            default:6
        }
    },
    template: ` 
 <div class="row justify-content-center">
                <div v-for="(item,index) in items" class="col-xl-4 col-lg-6 col-md-6 col-12" v-if="index < countPerPage">
                               <article class="blog-item" :class="{sticky:item.head.isTop}">
                                   <header class="blog-item-header">
                                       <a :href="item.head.imgTo">
                                           <img :src="item.head.img" :alt="item.head.alt">
                                       </a>
                                   </header>
                                   <div class="blog-item-body">
                                       <h4 class="blog-item-title">
                                        <a href="item.body.titleTo">
                                            <span :class="{'text-danger':item.head.isTop}">{{item.body.title|simple-show('9')}}</span>
                                        </a>
                                       </h4>
                                       <p :class="{'text-danger':item.head.isTop}">{{item.body.outline|simple-show('40')}}</p>
                                   </div>
                                   <footer class="blog-item-footer">
                                       <ul class="blog-item-meta">
                                            <li>
                                                <i class="fa fa-calendar-o fa-lg" :class="{'text-danger':item.head.isTop}"></i>
                                                <span :class="{'text-danger':item.head.isTop}">{{item.foot.writeDate|date-diff}}<span>
                                            </li>
                                            <li>
                                                <i class="fa fa-comments-o fa-lg" :class="{'text-danger':item.head.isTop}"></i>
                                                <span :class="{'text-danger':item.head.isTop}">{{item.foot.comments}}<span>
                                            </li>
                                            <li><i class="fa fa-thumbs-o-up fa-lg" :class="{'text-danger':item.head.isTop}"></i>
                                                <span :class="{'text-danger':item.head.isTop}">{{item.foot.favor}}<span>
                                            </li>
                                            <li><i class="fa fa-eye fa-lg" :class="{'text-danger':item.head.isTop}"></i>
                                                <span :class="{'text-danger':item.head.isTop}">{{item.foot.favor}}<span>
                                            </li>
                                       </ul>
                                   </footer>
                               </article>
                </div>
                
</div>
`
})

