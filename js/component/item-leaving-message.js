/**
 * 文章留言组件
 */
Vue.component('item-leaving-message', {
    props: {
        messages: {
            required: true,
            type: Array
        }
    },
    template: `
        <div class="commentlist section-padding-top-sm">
                                <h6 class="small-title">{{messages.length}}条留言</h6>
                                <template v-for="mess in messages">
                                    <div class="single-comment" :class="{'single-comment-reply':mess.replyForReply}">
                                        <div class="single-comment-thumb">
                                            <img :src="mess.userHeadImage" alt="hastech logo">
                                        </div>
                                        <div class="single-comment-content" >
                                            <div class="single-comment-content-top">
                                                <h6><span>{{mess.userName}}</span> – {{mess.userLeavingDatetime}}</h6>
                                                <a href="#" class="reply-button">Reply</a>
                                            </div>
                                            <p>{{mess.content}}</p>
                                        </div>
                                    </div>
                                </template>
                            </div>
    `
});
