/**
 * 留言图标组组件
 */
Vue.component("leaving-message-tip-group", {
    props: {
        item: {
            type: Object,
            required: true
        }
    },
    template: `
                                <div class="row">
                                    <div class="col-lg-4 col-3 text-left">
                                        <h5>{{item.datetime}}</h5>  
                                    </div>
                                    <div class="col-lg-2 col-3 text-center">
                                        <h5><i class="fa fa-comment-o"></i>11</h5>
                                    </div>
                                    <div class="col-lg-6 col-6 text-right">
                                        <h5><i class="fa fa-user-circle"></i>{{item.leavingUser}}</h5>
                                    </div>
                                </div>
    `,
    mounted(){
        cc.warn(JSON.stringify(this.item.datetime))
    }
})
