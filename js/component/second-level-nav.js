/**
 * 二级导航组件
 */
Vue.component('second-nav', {
    props:['items'],
    template: `
    <div class="breadcrumb-area bg-grey">
            <div class="container">
                <div class="cr-breadcrumb">
                    <h2>{{items.title}}</h2>    
                    <ul>
                        <li v-for="item in items.list">
                            <template v-if="item.link!==''">
                                <a :href="item.link">{{item.content}}</a>
                            </template>
                            <template v-else>
                                {{item.content}}
                            </template>
                        </li>
                    </ul>
                    
                </div>
            </div>
        </div>
    `
})
