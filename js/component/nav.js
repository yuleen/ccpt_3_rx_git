/**
 * 顶部导航组件
 */
Vue.component('top-nav', {
    data() {
        return {
            navList: [
                {order: 1, show: '主页', path: 'index.html'},
                {order: 2, show: '文章', path: 'article.html'},
                {order: 3, show: '留言', path: 'leaving-message.html'},
                {order: 4, show: '相册', path: 'photos.html'},
                {order: 5, show: '简历', path: 'about-me.html'},
                {order: 6, show: '心情', path: 'feeling.html'}
            ]
        }
    },
    methods: {
        toggleMenuIcon(e) {
            $(e.currentTarget).toggleClass('is-active');
            $('.main-navigation').toggleClass('is-visible');
        }
    },
    template: `
    <header class="header sticky-header fixed-header">
            <div class="container">
                <div class="header-inner d-none d-lg-flex">
                    <div class="header-logo">
                        <a href="index.html">
                            <img src="img/logo/logo-dark.png" alt="logo">
                        </a>
                    </div>
                    <div class="header-navigation">
                        <button class="header-navigation-trigger" @click="toggleMenuIcon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                        <nav class="main-navigation">
                            <ul>
                                <li v-for="nav in navList"><a :href="nav.path">{{nav.show}}</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="mobile-menu-wrapper d-block d-lg-none">
                    <div class="mobile-menu clearfix">
                        <a href="index.html" class="mobile-logo">
                            <img src="img/logo/logo-dark.png" alt="mobile logo">
                        </a>
                    </div>
                </div>
            </div>
        </header>
    `
})
