/**
 * 留言模块组件
 */
Vue.component('leaving-message-list', {
    props: {
        items: {
            type: Array,
            required: true
        },
        visibleCount: {
            type: Number,
            default: 6
        }
    },
    template: ` 
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-12" v-for="(item,index) in items" v-if="index < visibleCount">
                            <div class="service">
                                <p>{{item.outline|simple-show('58')}}</p>
                                <leaving-message-tip-group :item="item"></leaving-message-tip-group>
                            </div>
                        </div>
                    </div>
    `
})

