/**
 * 翻页组件
 */
Vue.component('page-helper', {
    template: `
                <div class="row pager-container">
                    <div class="col-lg-12">
                        <div class="pager-next text-center unselect">
                            <span class="fa fa-chevron-down" @click="$emit('load-more')">加载更多</span>
                        </div>
                    </div>
                </div>
    `
})
