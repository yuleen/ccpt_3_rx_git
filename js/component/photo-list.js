/**
 * 相册列表组件
 */
Vue.component('photo-list', {
    props: {
        items: {
            required: true,
            type: Array
        },
        countPerPage: {
            type: Number,
            default:6
        }
    },
    data() {
        return {
            lockedPic: 'img/portfolio/locked.jpg',
            lockedDefaultPassword: '123',
            showItems: false
        }
    },
    methods: {
        goPhoto(item) {
            if (item.needPassword) {
                let _this = this;
                layer.prompt({
                    formType: 1,
                    title: '\"' + item.title + '\"的访问密码:',
                }, function (value, index, elem) {
                    if (value === _this.lockedDefaultPassword) {
                        layer.close(index);
                        item.needPassword = false;
                    } else {
                        tip('密码错误');
                    }
                });
            }
        }
    },
    template: ` 
                <div class="row portfolios-wrapper portfolios-zoom-button-holder">
                        <div v-for="(item,index) in items" v-if="index < countPerPage" class="col-lg-4 col-md-6 col-12 portfolio-item portfolio-filter-graphic portfolio-filter-design">
                            <div class="portfolio" >
                                <div class="portfolio-image">
                                <template v-if="item.needPassword">
                                    <img :src="lockedPic" alt="图集已加密">
                                </template>
                                <template v-else>
                                    <img :src="item.smallPic" alt="portfolio image">
                                </template>
                                </div>
                                <div class="portfolio-content" :style="{top:item.needPassword?'0px':''}">
                                    <span @click="goPhoto(item)" class="portfolio-zoom-button"><i class="fa fa-2x" :class="item.needPassword?' fa-lock':' fa-link'" style="margin-left: 45%"></i></span>
                                    <h5>{{item.title|simple-show('7')}}</h5>
                                    <h6>{{item.datetime|date-diff}}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
`
})
