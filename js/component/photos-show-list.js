Vue.component("list-show", {
    props: {
        photoList: {
            type: Array,
            required: true
        }
    },
    template: `
            <div class="container show-container">
                <div class="row">
                    <span class="col-lg-3" v-for="photo in photoList">
                        <img :src="photo.src" class="mt-4" @click="$emit('img-click',photo)">
                    </span>
                </div>
            </div>
    `
})
