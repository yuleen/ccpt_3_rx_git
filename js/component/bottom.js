/**
 * 底部组件
 */
Vue.component('bottom', {
    template: `
    <footer class="footer-area bg-white text-center">
        <div class="powerBy container text-center">
            <H2>{{content}}</H2>
        </div>
        <div class="link-door container text-center">
            <div class="row link-door-title">
                <div class="col-lg-12"><h3>传送门</h3></div>
            </div>
            <div class="row link-door-list">
                <div class="col-lg-2"><a href="#">码云</a></div>
                <div class="col-lg-2"><a href="#">Github</a></div>
                <div class="col-lg-2"><a href="#">QQ</a></div>
                <div class="col-lg-2"><a href="#">JQ22</a></div>
                <div class="col-lg-2"><a href="#">阿里云</a></div>
                <div class="col-lg-2"><a href="#">Vuejs</a></div>
            </div>
        </div>
    </footer>
    `,
    data() {
        return {
            content: "Powered by Leen"
        }
    }
})
