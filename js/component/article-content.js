/**
 * 文章内容组件
 */
Vue.component('article-content', {
    props: ['item','messages'],
    template: ` 
<div class="cr-section blog-area section-padding-lg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="blog-details">
                            <article class="blog-item sticky">
                                <div class="blog-item-header">
                                    <img src="img/blog/blog-details/blog-image-1.jpg" alt="blog thumb">
                                </div>
                                <div class="blog-item-body">
                                    <h3 class="blog-item-title">{{item.body.title}}</h3>
                                    <h5>{{item.foot.writeDate}}</h5>
                                    <p>Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum
                                        itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio
                                        accusantium qui deleniti nihil harum itaque. Aspernatur debitis volu necessi
                                        tatibus omnis est provident odio it nam rerum sed aut est et. Ad quo minima. Rem
                                        ut voluptate nihil suscipit quis impedit dolores doloribus. Sed distinctio
                                        ducimus nemo laboriosam itaque. Est id eius perspiciatis sequi ullam. Corrupti
                                        nihil non ut in accusantium animi alias quaerat. Ipsum commodi adipisci.
                                        Consectetur ipsa qui autem quod molestiae deserunt sit et. Fugiat dolore sunt.
                                        Natus ex ea laboriosam iste doloremque aut quia. Unde repellendus et et
                                        quibusdam dicta provident recusandae quia. Maiores voluptatem iure ad animi et.
                                        Consequatur voluptatem eos. Unde repellendus et et quibusdam dicta provident
                                        recusandae quia. Maiores voluptatem iure ad animi et. Consequatur voluptatem
                                        eos.
                                    </p>
                                    <blockquote>
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente aperiam
                                            minima libero perspiciatis sequi pariatur Distinctio aut sed vel. Dolorum
                                            voluptates et ex commodi.
                                        </p>
                                    </blockquote>
                                    <p>Sit nam rerum sed aut est et. Numquam non accusantium qui deleniti nihil harum
                                        itaque. Aspernatur debitis volu necessi tatibus omnis est provident odio
                                        accusantium qui deleniti nihil harum itaque. Aspernatur debitis volu necessi
                                        tatibus omnis est provident odio it nam rerum sed aut est et. Ad quo minima. Rem
                                        ut voluptate nihil suscipit quis impedit dolores doloribus. Sed distinctio
                                        ducimus nemo laboriosam itaque. Est id eius perspiciatis sequi ullam. Corrupti
                                        nihil non ut in accusantium animi alias quaerat. Ipsum commodi adipisci.
                                        Consectetur ipsa qui autem quod molestiae deserunt sit et. Fugiat dolore sunt.
                                        Natus ex ea laboriosam iste doloremque aut quia. Unde repellendus et et
                                        quibusdam dicta provident recusandae quia. Maiores voluptatem iure ad animi et.
                                        Consequatur voluptatem eos. Unde repellendus et et quibusdam dicta provident
                                        recusandae quia. Maiores voluptatem iure ad animi et. Consequatur voluptatem
                                        eos.
                                    </p>
                                </div>
                            </article>
                            <div class="blog-details-bottom">
                                <div class="blog-details-tags">
                                    <h6>标签</h6>
                                    <ul>
                                        <li><a href="blog-right-sidebar.html">设计</a></li>
                                        <li><a href="blog-right-sidebar.html">开发</a></li>
                                        <li><a href="blog-right-sidebar.html">创作素材</a></li>
                                    </ul>
                                </div>
                                <div class="blog-details-share">
                                    <ul>
                                        <li>
                                            <span class="mouse-pointer">
                                                <i class="fa fa-share-alt fa-lg"></i>
                                                <span>10<span>
                                            </span>
                                             
                                        </li>
                                        <li>
                                            <span class="mouse-pointer">
                                                    <i class="fa fa-thumbs-o-up fa-lg"></i>
                                                    <span>{{item.foot.favor}}<span>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="mouse-pointer">
                                                    <i class="fa fa-eye fa-lg"></i>
                                                    <span>{{item.foot.favor}}<span>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <item-leaving-message :messages="messages"></item-leaving-message>
                            <leaving-message-form></leaving-message-form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    mounted(){

    }
})

