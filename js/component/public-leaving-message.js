/**
 * 公共留言模块组件
 */
Vue.component('public-leaving-message', {
    props: {
        list: {
            type: Array,
            required: true
        }
    },
    template: `
            <div class="container leaving-message-container">
                <template v-for="item in list">
                    <template v-if="item.main">
                        <div class="row reply">
                            <span class="col-lg-2 text-right message-author">
                                <img src="img/author-image/author-image-1.png">
                            </span>
                            <span class="col-lg-10 message-span text-left">{{item.content}}</span>
                        </div>
                    </template>
                    <template v-else>
                        <div class="row sub-reply">
                            <span class="col-lg-3 text-right message-author">
                                <img src="img/author-image/author-image-2.png">
                            </span>
                            <span class="col-lg-9 message-span text-left">{{item.content}}</span>
                        </div>
                    </template>
                    <div class="row replay-tip">
                            <span class="col-lg-8"></span>
                            <span class="col-lg-4">
                                <leaving-message-tip-group :item="item.author"></leaving-message-tip-group>
                            </span>
                        </div>
                </template>
             </div>
    `
});
