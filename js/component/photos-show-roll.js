var rs = Vue.component("roll-show", {
    props:{
        imageObj:{
            type:Object,
            required:true
        }
    },
    template: `
                    <div class="container show-container">
                        <div class="portfolio-details-images">
                              <img :src="imageObj.src" class="mt-4" @mousemove="toggleMouseIcon" @click="$emit('img-click')" alt="Portfolio Details Image">
                        </div>
                        <div class="row section-padding-top-sm">
                            <div class="col-lg-4">
                                <div class="portfolio-details-meta">
                                    <ul>
                                        <li><i class="fa fa-calendar"></i><span>日期:</span>{{imageObj.datetime|date-diff}}</li>
                                        <li><i class="fa fa-eye"></i><span>浏览:</span>{{imageObj.view}}</li>
                                        <li><i class="fa fa-lock"></i><span>类型:</span>{{imageObj.lock?'加密':'公开'}}</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="portfolio-details-info">
                                    <h3 class="small-title-fullwidth"><span>{{imageObj.photoGroup}}</span></h3>
                                    <h5 class="portfolio-title">{{imageObj.photoName}}</h5>
                                    <p>{{imageObj.description}}</p>
                                    <div class="post-share">
                                        <h6>标签:</h6>
                                        <ul>
                                            <li v-random-bg-tag v-for="tag in imageObj.tags">{{tag}}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    `,
    methods: {
        toggleMouseIcon(obj) {
            this.$parent.psr.cursorOffset = obj.offsetX;
            this.$parent.psr.eventObject = obj.target;
        }
    }
})
