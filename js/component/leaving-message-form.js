/**
 * 提交留言组件
 */
Vue.component('leaving-message-form', {
    template: `
        <div class="blog-details-commentbox section-padding-top-xlg">
                                <h6 class="small-title">留言:</h6>
                                <form action="#" class="karigor-form">
                                    <div class="karigor-form-inner">
                                        <div class="karigor-form-input">
                                            <textarea id="new-review-textbox" cols="30" rows="5" placeholder="内容"></textarea>
                                        </div>
                                        <div class="karigor-form-input">
                                            <input type="text" placeholder="手机号码">
                                        </div>
                                        <div class="karigor-form-input">
                                            <button type="submit" class="button">
                                                <span>提交留言</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
    `
});
