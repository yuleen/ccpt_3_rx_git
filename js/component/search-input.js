/**
 * 搜索组件
 */
Vue.component("search", {
    template: `
            <div class="row">
                <div class="col-sm-6 col-lg-10">
                    <input class="form-control" @keyup.enter="$emit('search')" placeholder="感兴趣的内容..." type="text">
                </div>
                <div class="col-sm-6 col-lg-2 text-center">
                    <button class="btn btn-secondary fa fa-search margin-top">Search</button>
                </div>
            </div>
    `
})
