Vue.directive('random-bg-tag', {
    bind(el, binding, vnode) {
        let badgeArray = ['badge-primary', 'badge-secondary', 'badge-success', 'badge-info', 'badge-warning', 'badge-danger', 'badge-light', 'badge-dark'];
        let num = Math.round(Math.random() * (7 - 0) + 0)
       el.className=badgeArray[num];
    }
})
