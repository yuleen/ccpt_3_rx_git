function dateTimeDiff(startTime, endTime) {
    let str = "";
    var sTime = new Date(startTime);
    var eTime = new Date(endTime);
    var diff = eTime - sTime;
    if (diff >= 0) {
        const lessOneMinute = 60 * 1000;
        if (diff <= lessOneMinute) {
            str = '刚刚';
        } else {
            const lessOneHour = 60 * lessOneMinute;
            if (diff <= lessOneHour) {
                let calcMinute = Math.floor(diff / lessOneMinute);
                str = calcMinute + "分钟前";
            } else {
                const lessOneDay = 24 * lessOneHour;
                if (diff < lessOneDay) {
                    let calcHour = Math.floor(diff / lessOneHour);
                    str = calcHour + "小时前";
                } else {
                    const lessOneMonth = 30 * lessOneDay;
                    if (diff < lessOneMonth) {
                        let calcDay = Math.floor(diff / lessOneDay);
                        str = calcDay + "天前";
                    } else {
                        const lessOneYear = 12 * lessOneMonth;
                        if (diff < lessOneYear) {
                            let calcMonth = Math.floor(diff / lessOneMonth);
                            str = calcMonth + "个月前";
                        } else {
                            let calcYear = Math.floor(diff / lessOneYear);
                            str = calcYear + "年前";
                        }
                    }
                }
            }
        }
    } else {
        console.warn("操作失败:结束时间小于其实时间")
    }
    return str;
}

function getLocalDateTime(fmt) {
    var date = new Date();
    var o = {
        "M+": date.getMonth() + 1,     //月份
        "d+": date.getDate(),     //日
        "h+": date.getHours(),     //小时
        "m+": date.getMinutes(),     //分
        "s+": date.getSeconds(),     //秒
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度
        "S": date.getMilliseconds()    //毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}


