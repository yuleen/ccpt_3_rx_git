/**
 * 时间过滤器
 */
Vue.filter('date-diff', function (arg1) {
    if (arg1) {
        return dateTimeDiff(arg1,getLocalDateTime('yyyy-MM-dd hh:mm:ss'))
    }
})
