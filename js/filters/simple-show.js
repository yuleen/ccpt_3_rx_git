/**
 * 简要显示过滤器
 */
Vue.filter('simple-show', function (arg1, arg2) {
    if (arg1 && arg2 && !isNaN(arg2)) {
        if (arg1.length > arg2) {
            return arg1.substring(0, arg2) + "...";
        } else {
            return arg1;
        }
    } else {
        cc.error('参数必须是数字!');
    }
})
